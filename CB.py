import bpy
from random import randint

class MCN_OT_CB(bpy.types.Operator):
    """Spawn a cone in the scene"""
    bl_idname = "object.spawn_cone"
    bl_label = "Spawn Cone"  # nom apparaissant dans l'interface
    bl_options = {'REGISTER', 'UNDO'}

    def execute(self, context):
        name = context.scene.name
        size = randint(1,5)
        x = randint(-10,10)
        y = randint(-10,10)
        z = randint(-10,10)
        bpy.ops.mesh.primitive_cone_add(location=(x,y,z))
        bpy.context.object.name = name
        
        return {'FINISHED'}

class MCN_PT_CB(bpy.types.Panel):
    """"""
    bl_label = "CB’s button"
    bl_idname = "MCN_PT_CB"
    bl_space_type = 'VIEW_3D'
    bl_region_type = 'UI'
    bl_category = "Tool"
    bl_parent_id = "MCN_PT_Main_Panel"
    bl_options = {'DEFAULT_CLOSED'}

    def draw(self, context):
        layout = self.layout

        # TODO Replace this label by your stuff here
        layout.label(text="CB’s tools here")
        
        layout = self.layout
        layout.use_property_split = True

        col = layout.column(align=True)
        col.prop(context.scene, "name")

        col = layout.column()
        col.operator("object.spawn_cone")


def register():
    bpy.utils.register_class(MCN_PT_CB)
    bpy.utils.register_class(MCN_OT_CB)
    
    bpy.types.Scene.spawn_cone = bpy.props.StringProperty(name='Objects name',
                                                           description='String used to name the object',
                                                           default='Default_Cone')


def unregister():
    bpy.utils.unregister_class(MCN_PT_CB)
    bpy.utils.unregister_class(MCN_OT_CB)